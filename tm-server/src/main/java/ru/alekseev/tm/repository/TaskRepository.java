package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    private Connection connection;
    public TaskRepository(Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setDateStart(row.getDate("dateBegin"));
        task.setDateFinish(row.getDate("dateEnd"));
        task.setCreatedOn(row.getDate("createdOn"));
        String stringStatus = row.getString("task_status");
        task.setStatus(Status.getStatusFromString(stringStatus));
        return task;
    }

    @Override
    @Nullable
    @SneakyThrows
    public final Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ? AND id = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, taskId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Task task = fetch(resultSet);
            preparedStatement.close();
            return task;
        }
        resultSet.close();
        preparedStatement.close();
        return null;
    }

    @Override
    @NotNull
    @SneakyThrows
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public final void persist(@NotNull final Task task) {
        @NotNull final String query = "INSERT INTO app_task (id, dateBegin, dateEnd, createdOn, description, name, user_id, project_id, task_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setDate(2, null);
        preparedStatement.setDate(3, null);
        preparedStatement.setDate(4, null);
        preparedStatement.setString(5, task.getDescription());
        preparedStatement.setString(6, task.getName());
        preparedStatement.setString(7, task.getUserId());
        preparedStatement.setString(8, task.getProjectId());
        preparedStatement.setString(9, task.getStatus().toString());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void updateByNewData(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        @NotNull final String query = "UPDATE app_task SET name = ? WHERE user_id = ? AND id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, userId);
        preparedStatement.setString(3, taskId);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void delete(@NotNull String id) {
        @NotNull final String query = "DELETE FROM app_task WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final String query = "DELETE FROM app_task WHERE id = ? AND user_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, taskId);
        preparedStatement.setString(2, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void clearByProjectId(@NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM app_task WHERE project_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void clearByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM app_task WHERE user_id = ? AND project_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.execute();
        preparedStatement.close();
    }
}

package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    private Connection connection;
    public SessionRepository(Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    private Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setUserId(row.getString("user_id"));
        session.setSignature(row.getString("signature"));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session open(String userId) {
        Session newSession = new Session();
        newSession.setUserId(userId);
        String signature = SignatureUtil.sign(newSession);
        newSession.setSignature(signature);
        @NotNull final String query = "INSERT INTO app_session (id, user_id, signature) VALUES (?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, newSession.getId());
        //preparedStatement.setLong(2, newSession.getTimestamp());
        preparedStatement.setString(2, newSession.getUserId());
        preparedStatement.setString(3, newSession.getSignature());
        preparedStatement.execute();
        preparedStatement.close();
        return newSession;
    }

    @Override
    @SneakyThrows
    public void closeSession(String sessionId) {
        @NotNull final String query = "DELETE FROM app_session WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        preparedStatement.execute();
        preparedStatement.close();
    }
}

package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    private Connection connection;
    public UserRepository(Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    private User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHashcode(row.getString("passwordHash"));
        user.setEmail(row.getString("email"));
        String stringRoleType = row.getString("role");
        user.setRoleType(RoleType.getRoleTypeFromString(stringRoleType));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User findOne(@NotNull String id) {
        @NotNull final String query = "SELECT * FROM app_user WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            @NotNull final User user = fetch(resultSet);
            preparedStatement.close();
            return user;
        }
        preparedStatement.close();
        return null;
    }

    @Override
    @Nullable
    @SneakyThrows
    public final User findOneByLoginAndPasswordHashcode(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ? AND passwordHash = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, passwordHashcode);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            @NotNull final User user = fetch(resultSet);
            resultSet.close();
            preparedStatement.close();
            return user;
        }
        resultSet.close();
        preparedStatement.close();
        return null;
    }

    @Override
    @SneakyThrows
    public final void persist(@NotNull final User user) {
        @NotNull final String query = "INSERT INTO app_user (id, email, login, passwordHash, role) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getEmail());
        preparedStatement.setString(3, user.getLogin());
        preparedStatement.setString(4, user.getPasswordHashcode());
        preparedStatement.setString(5, user.getRoleType().toString());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void merge(@NotNull User user) {
        @NotNull final String query = "UPDATE app_user SET email = ?, login = ?, passwordHash = ?, role = ? WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getEmail());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPasswordHashcode());
        preparedStatement.setString(4, user.getRoleType().toString());
        preparedStatement.setString(5, user.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        @NotNull final String query = "UPDATE app_user SET passwordHash = ? WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, passwordHashcode);
        preparedStatement.setString(2, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void updateUserByRole(@NotNull final String userId, @NotNull final RoleType roleType) {
        @NotNull final String query = "UPDATE app_user SET role = ? WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, roleType.toString());
        preparedStatement.setString(2, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void deleteByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        @NotNull final String query = "DELETE FROM app_user WHERE login = ? AND passwordHash = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, passwordHashcode);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }
}

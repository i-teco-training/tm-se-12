package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    private Connection connection;
    public ProjectRepository(Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setUserId(row.getString("user_id"));
        project.setDateStart(row.getDate("dateBegin"));
        project.setDateFinish(row.getDate("dateEnd"));
        project.setCreatedOn(row.getDate("createdOn"));
        String stringStatus = row.getString("project_status");
        project.setStatus(Status.getStatusFromString(stringStatus));

        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOne(@NotNull String id) { //нужен метод???
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Project project = fetch(resultSet);
            preparedStatement.close();
            return project;
        }
        resultSet.close();                        //correct???
        preparedStatement.close();
        return null;
    }

    @Override
    @Nullable
    @SneakyThrows
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ? AND id = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Project project = fetch(resultSet);
            preparedStatement.close();
            return project;        }
        resultSet.close();
        preparedStatement.close();
        return null;
    }

    @Override
    @NotNull
    @SneakyThrows
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ?";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public final void persist(@NotNull final Project project) {
        @NotNull final String query = "INSERT INTO app_project (id, dateBegin, dateEnd, createdOn, description, name, user_id, project_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setDate(2, null);
        preparedStatement.setDate(3, null);
        preparedStatement.setDate(4, null);
        preparedStatement.setString(5, project.getDescription());
        preparedStatement.setString(6, project.getName());
        preparedStatement.setString(7, project.getUserId());
        preparedStatement.setString(8, project.getStatus().toString());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void updateByUserIdProjectIdProjectName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        @NotNull final String query = "UPDATE app_project SET name = ? WHERE user_id = ? AND id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectName);
        preparedStatement.setString(2, userId);
        preparedStatement.setString(3, projectId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public void delete(@NotNull String id) {
        @NotNull final String query = "DELETE FROM app_project WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void deleteByUserId(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM app_project WHERE user_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @SneakyThrows
    public final void deleteProjectByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM app_project WHERE id = ? AND user_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        preparedStatement.setString(2, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }
}

package ru.alekseev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public final class Session extends AbstractEntity implements Cloneable {
    @NotNull private String id = UUID.randomUUID().toString();
    @NotNull  private Long timestamp = new Date().getTime();
    @Nullable private String userId;
    @Nullable private String signature;

    @Override
    @Nullable
    public final Session clone() {
        try {
            return (Session)super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}

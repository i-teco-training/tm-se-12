package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
    private ServiceLocator serviceLocator;

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Project findOneProjectByUserIdAndProjectId(
            @WebParam @NotNull final String userId,
            @WebParam @NotNull final String projectId
    ) {
        return serviceLocator.getProjectService().findOneByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @WebMethod
    public List<Project> findAllProjects() {
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @WebMethod
    public List<Project> findAllProjectsByUserId(@WebParam @NotNull final Session session) {
        System.out.println("session.id -> " + session.getUserId());
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void addProjectByUserIdProjectName(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String projectName
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().addProjectByUserIdProjectName(session.getUserId(), projectName);
    }

    @Override
    @WebMethod
    public void updateProject(@WebParam @NotNull final Project entity) {
        serviceLocator.getProjectService().update(entity);
    }

    @Override
    @WebMethod
    public void updateProjectByProjectIdProjectName(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String projectId,
            @WebParam @NotNull final String name
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().updateByUserIdProjectIdProjectName(session.getUserId(), projectId, name);
    }

    @Override
    @WebMethod
    public void deleteProject(@WebParam @NotNull final String id) {
        serviceLocator.getProjectService().delete(id);
    }

    @Override
    @WebMethod
    public void deleteProjectByProjectId(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String projectId
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().deleteByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void clearProjects() {
        serviceLocator.getProjectService().clear();
    }

    @Override
    @WebMethod
    public void clearProjectsByUserId(@WebParam @NotNull final String userId) {
        serviceLocator.getProjectService().clearByUserId(userId);
    }
}

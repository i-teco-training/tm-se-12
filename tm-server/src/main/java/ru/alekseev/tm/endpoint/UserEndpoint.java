package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IUserEndpoint;
import ru.alekseev.tm.api.iservice.IUserService;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.util.HashUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {
    private ServiceLocator serviceLocator;

    public UserEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public User findOneUserByLoginAndPassword(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String passwordHashcode
    ) {
        return serviceLocator.getUserService().findOneByLoginAndPassword(login, passwordHashcode);
    }

    @Override
    @Nullable
    @WebMethod
    public final User findOneUser(@WebParam @NotNull final Session session) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return null;
        }
        return serviceLocator.getUserService().findOne(session.getId());
    }

    @Override
    @WebMethod
    public List<User> findAllUsers() {
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public void addUser(@WebParam @NotNull final User entity) {
        serviceLocator.getUserService().add(entity);
    }

    @Override
    @WebMethod
    public void addUserByLoginAndPassword(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String password
    ) {
        String passwordHashcode = HashUtil.getMd5(password);
        serviceLocator.getUserService().addByLoginAndPassword(login, passwordHashcode);
    }

    @Override
    @WebMethod
    public void addUserByLoginPasswordUserRole(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String passwordHashcode,
            @WebParam @NotNull final RoleType roleType
    ) {
        serviceLocator.getUserService().addByLoginPasswordUserRole(login, passwordHashcode, roleType);
    }

    @Override
    @WebMethod
    public void updateUser(@WebParam @NotNull final User entity) {
        serviceLocator.getUserService().update(entity);
    }

    @Override
    @WebMethod
    public void updateUserPassword(@WebParam @NotNull final Session session, @WebParam @NotNull final String password) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        String passwordHashcode = HashUtil.getMd5(password);
        serviceLocator.getUserService().updateUserPassword(session.getUserId(), passwordHashcode);
    }

    @Override
    @WebMethod
    public void updateUserByRole(@WebParam @NotNull final Session session, @WebParam @NotNull final RoleType roleType) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getUserService().updateUserByRole(session.getUserId(), roleType);
    }

    @Override
    @WebMethod
    public void deleteUser(@WebParam @NotNull final String id) {
        serviceLocator.getUserService().delete(id);
    }

    @Override
    @WebMethod
    public void deleteUserByLoginAndPassword(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String passwordHashcode
    ) {
        serviceLocator.getUserService().deleteByLoginAndPassword(login, passwordHashcode);
    }

    @Override
    @WebMethod
    public void clearUsers() {
        serviceLocator.getUserService().clear();
    }
}

package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.api.iservice.ITaskService;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.util.DBConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {
    @Override
    @Nullable
    public final Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            connection.commit();
            connection.close();
            return taskRepository.findOneByUserIdAndTaskId(userId, taskId);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    @Nullable
    public final List<Task> findAll() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            connection.commit();
            connection.close();
            return taskRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    @Nullable
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            connection.commit();
            connection.close();
            return taskRepository.findAllByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public final void add(@NotNull final Task task) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.persist(task);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void addTaskByUserIdTaskName(@NotNull final String userId, @NotNull final String taskName) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(taskName);
        add(task);
    }

    @Override
    public final void update(@NotNull final Task task) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.merge(task);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void updateByNewData(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        if (userId.isEmpty() || taskId.isEmpty() || name.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.updateByNewData(userId, taskId, name);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        if (id.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.delete(id);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        if (userId.isEmpty() || taskId.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.deleteByUserIdAndTaskId(userId, taskId);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clear() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.clear();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clearByProjectId(@NotNull final String projectId) {
        if (projectId.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.clearByProjectId(projectId);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clearByUserIdProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
        try {
            taskRepository.clearByUserIdAndProjectId(userId, projectId);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
}

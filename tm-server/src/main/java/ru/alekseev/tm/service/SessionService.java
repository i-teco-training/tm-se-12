package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.api.iservice.ISessionService;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.repository.SessionRepository;
import ru.alekseev.tm.util.DBConnectionUtil;
import ru.alekseev.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {
    @Override
    public final void add(@NotNull final Session entity) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.persist(entity);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    @Nullable
     public final List<Session> findAll() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            connection.commit();
            connection.close();
            return sessionRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public final void update(@NotNull final Session entity) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.merge(entity);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.closeSession(id);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clear() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            sessionRepository.clear();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final Session open(@NotNull final String userId) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);
        try {
            @Nullable Session session = sessionRepository.open(userId);
            connection.commit();
            connection.close();
            return session;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        if (session == null) return false;
        if (session.getSignature() == null || session.getSignature().isEmpty()) return false;
        if (session.getUserId() == null || session.getUserId().isEmpty()) return false;
        if (session.getTimestamp() == null) return false;
        @NotNull final String sourceSignature = session.getSignature();
        session.setSignature(null);
        @NotNull final String targetSignature = SignatureUtil.sign(session);
        //if (sessionRepository.findOne(session.getId()) == null) return false;
        return sourceSignature.equals(targetSignature);
    }
}

package ru.alekseev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.api.iservice.IUserService;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.util.DBConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Getter
@Setter
public class UserService extends AbstractService<User> implements IUserService {
    @Override
    @Nullable
    public final User findOne(@NotNull final String id) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            @Nullable User user = userRepository.findOne(id);
            connection.commit();
            connection.close();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    @Nullable
    public final User findOneByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return null;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            @Nullable User user = userRepository.findOneByLoginAndPasswordHashcode(login,passwordHashcode);
            connection.commit();
            connection.close();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    @NotNull
    public final List<User> findAll() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            connection.commit();
            connection.close();
            return userRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public final void add(@NotNull final User user) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.persist(user);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty() || roleType.toString().isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        user.setRoleType(roleType);
        add(user);
    }

    @Override
    public final void addByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        add(user);
    }

    @Override
    public final void update(@NotNull final User user) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.merge(user);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.updateUserPassword(userId, passwordHashcode);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void updateUserByRole(String userId, RoleType roleType) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.updateUserByRole(userId, roleType);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.delete(id);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void deleteByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.deleteByLoginAndPassword(login, passwordHashcode);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clear() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        try {
            userRepository.clear();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
}

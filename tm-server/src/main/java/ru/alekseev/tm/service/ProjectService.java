package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.api.iservice.IProjectService;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.util.DBConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    @Override
    @Nullable
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @Nullable final Project project = projectRepository.findOneByUserIdAndProjectId(userId, projectId);
            connection.commit();
            connection.close();
            return project;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Override
    @Nullable
    public final List<Project> findAll() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
        try {
            connection.commit();
            connection.close();
            return projectRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    @Nullable
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        @NotNull List<Project> projects = new ArrayList<>();
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {@NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projects = projectRepository.findAllByUserId(userId);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
                try {
                    connection.rollback();
                    connection.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        return projects;
    }

    @Override
    public final void add(@NotNull final Project project) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.persist(project);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void addProjectByUserIdProjectName(String userId, String projectName) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(projectName);
        add(project);
    }

    @Override
    public final void update(@NotNull final Project project) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.merge(project);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void updateByUserIdProjectIdProjectName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        if (userId.isEmpty() || projectId.isEmpty() || projectName.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.updateByUserIdProjectIdProjectName(userId, projectId, projectName);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.delete(id);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void deleteByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @Nullable Project projectForExistenceChecking = projectRepository.findOne(projectId);
            if (projectForExistenceChecking.getUserId() == null || projectForExistenceChecking.getUserId().isEmpty())
                return;
            if (!userId.equals(projectForExistenceChecking.getUserId())) return;
            delete(projectId);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clear() {
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.clear();
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public final void clearByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        @Nullable final Connection connection = DBConnectionUtil.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.deleteByUserId(userId);
            connection.commit();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
}

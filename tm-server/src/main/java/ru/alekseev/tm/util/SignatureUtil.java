package ru.alekseev.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class SignatureUtil {
    @Nullable
    public static String sign(@Nullable final Object value) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }
    @Nullable
    public static String sign(@Nullable final String value) {
        @NotNull String salt = "elonmusk";
        @NotNull final Integer cycle = 9;
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.getMd5(salt + result + salt);
        }
        return result;
    }
}

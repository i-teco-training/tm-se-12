package ru.alekseev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnectionUtil {
    @NotNull private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    @NotNull private static final String url = "jdbc:mysql://localhost:3306/alekseev2";
    @NotNull private static final String user = "root";
    @NotNull private static final String password = "root";

    public static Connection getConnection() {
        @Nullable Connection connection = null;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(url, user, password);
            connection.setAutoCommit(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}

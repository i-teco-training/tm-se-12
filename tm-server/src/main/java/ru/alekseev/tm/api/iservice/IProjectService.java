package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project findOneByUserIdAndProjectId(String userId, String projectId);

    List<Project> findAllByUserId(String userId);

    void addProjectByUserIdProjectName(String userId, String projectName);

    void updateByUserIdProjectIdProjectName(String userId, String projectId, String projectName);

    void deleteByProjectId(String userId, String projectId);

    void clearByUserId(String userId);
}

package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.dto.Domain;

public interface IDomainService {

    Domain getDomain();

    void setDomain(Domain domain);
}

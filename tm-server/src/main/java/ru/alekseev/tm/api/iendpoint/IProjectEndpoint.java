package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Session;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    Project findOneProjectByUserIdAndProjectId(String userId, String projectId);

    List<Project> findAllProjects();

    List<Project> findAllProjectsByUserId(Session session);

    void addProjectByUserIdProjectName(Session session, String projectName);

    void updateProject(Project entity);

    void updateProjectByProjectIdProjectName(Session session, String projectId, String name);

    void deleteProjectByProjectId(Session session, String projectId);

    void deleteProject(String id);

    void clearProjects();

    void clearProjectsByUserId(String userId);
}

package ru.alekseev.tm.api.irepository;

import ru.alekseev.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    Session open(String userId);

    void closeSession(String sessionId);
}

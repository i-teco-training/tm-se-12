package ru.alekseev.tm.api.iendpoint;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    User findOneUserByLoginAndPassword(String login, String passwordHashcode);

    User findOneUser(@NotNull final Session session);

    List<User> findAllUsers();

    void addUser(User entity);

    void addUserByLoginAndPassword(String login, String passwordHashcode);

    void addUserByLoginPasswordUserRole(String login, String passwordHashcode, RoleType roleType);

    void updateUser(User entity);

    void updateUserPassword(Session session, String passwordHashcode);

    void updateUserByRole(Session session, RoleType roleType);

    void deleteUser(String id);

    void deleteUserByLoginAndPassword(String login, String passwordHashcode);

    void clearUsers();
}

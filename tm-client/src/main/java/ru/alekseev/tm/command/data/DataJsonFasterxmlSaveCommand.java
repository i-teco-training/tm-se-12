package ru.alekseev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.IDomainEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.File;
import java.io.FileWriter;

public final class DataJsonFasterxmlSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "json2-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to FasterXML JSON";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE TO FASTERXML JACKSON JSON]");
        @NotNull final IDomainEndpoint domainEndpoint =
                serviceLocator.getDomainEndpointService().getDomainEndpointPort();
        @Nullable final Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToJson = domainEndpoint.getDomain(currentSession);
        if (domainToJson == null) return;

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String jsonString =
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domainToJson);
        @NotNull final File jsonOutput = new File(filePath);
        @NotNull final FileWriter fileWriter = new FileWriter(jsonOutput);
        fileWriter.write(jsonString);
        fileWriter.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}
package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.RoleType;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.User;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class UserEditCurrentProfileCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "edit-profile";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Edit your Project Manager profile";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[EDITING YOUR PROFILE]");
        Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("If you want to get ADMIN account type enter \"1\"");
        @NotNull final String accountTypeUpdateChoice = serviceLocator.getTerminalService().getFromConsole();
        if ("1".equals(accountTypeUpdateChoice)) {
            RoleType roleType = RoleType.ADMIN;
            serviceLocator.getUserEndpointService().getUserEndpointPort().updateUserByRole(currentSession, roleType);
        }
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}

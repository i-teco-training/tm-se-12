package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.api.iendpoint.Project;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectSortCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "sort-projects";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show sorted list of all projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SORTED LIST OF ALL PROJECTS]");
        @Nullable final Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @NotNull final IProjectEndpoint projectEndpoint =
                serviceLocator.getProjectEndpointService().getProjectEndpointPort();

        @Nullable final List<Project> listForSorting = projectEndpoint.findAllProjectsByUserId(currentSession);

        if (listForSorting.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
            return;
        }
        System.out.println("sort by?");
        System.out.println("1 - by date of creation, 2 - by start, 3 - by finish, 4 - by status");
        System.out.println("ENTER 1, 2, 3 or 4");
        @NotNull final String input = serviceLocator.getTerminalService().getFromConsole();

        switch (input) {
            case "1": Collections.sort(listForSorting, new CreatedOnComparator()); break;
            case "2": Collections.sort(listForSorting, new DateStartComparator()); break;
            case "3": Collections.sort(listForSorting, new DateFinishComparator()); break;
            case "4": Collections.sort(listForSorting, new StatusComparator()); break;
            default:
                System.out.println("Incorrect input. Try again.");
                return; //тут нужен break?
        }

        for (int i = 0; i < listForSorting.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + listForSorting.get(i).getName()
                    + ", projectId: " + listForSorting.get(i).getId()
                    + ", date of creation: " + listForSorting.get(i).getCreatedOn()
                    + ", start date: " + listForSorting.get(i).getDateStart()
                    + ", finish date: " + listForSorting.get(i).getDateFinish()
                    + ", status: " + listForSorting.get(i).getStatus());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    public final class CreatedOnComparator implements Comparator<Project> {
        @Override
        public int compare(@NotNull final Project o1, @NotNull final Project o2) {
            return (int) (o1.getCreatedOn().toGregorianCalendar().getTime().getTime() -
                    o2.getCreatedOn().toGregorianCalendar().getTime().getTime());
        }
    }

    public final class DateStartComparator implements Comparator<Project> {
        @Override
        public int compare(@NotNull final Project o1, @NotNull final Project o2) {
            if (o1.getDateStart() == null) return -1;
            if (o2.getDateStart() == null) return 1;
            return (int) (o1.getDateStart().toGregorianCalendar().getTime().getTime() -
                    o2.getDateStart().toGregorianCalendar().getTime().getTime());
        }
    }

    public final class DateFinishComparator implements Comparator<Project> {
        @Override
        public int compare(@NotNull final Project o1, @NotNull final Project o2) {
            if (o1.getDateFinish() == null) return -1;
            if (o2.getDateFinish() == null) return 1;
            return (int) (o1.getDateFinish().toGregorianCalendar().getTime().getTime() -
                    o2.getDateFinish().toGregorianCalendar().getTime().getTime());
        }
    }

    public final class StatusComparator implements Comparator<Project> {
        @Override
        public int compare(@NotNull final Project o1, @NotNull final Project o2) {
            return o1.getStatus().ordinal() - o2.getStatus().ordinal();
        }
    }

}